package com.example.myrecyclerview;

public class FoodModel {
    private String foodName;
    private int price;
    private String restuarant;
    private String type;
    private String imageUrl;
    private String htChars = "";


    public String getHtChars() {
        return htChars;
    }

    public void setHtChars(String htChars) {
        this.htChars = htChars;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getRestuarant() {
        return restuarant;
    }

    public void setRestuarant(String restuarant) {
        this.restuarant = restuarant;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private FoodModel(Builder builder) {
        foodName = builder.foodName;
        price = builder.price;
        restuarant = builder.restuarant;
        type = builder.type;
        imageUrl = builder.imageUrl;
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public static final class Builder {
        private String foodName;
        private int price;
        private String restuarant;
        private String type;
        private String imageUrl;

        private Builder() {
        }

        public Builder foodName(String val) {
            foodName = val;
            return this;
        }

        public Builder price(int val) {
            price = val;
            return this;
        }

        public Builder restuarant(String val) {
            restuarant = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder imageUrl(String val) {
            imageUrl = val;
            return this;
        }

        public FoodModel build() {
            return new FoodModel(this);
        }
    }
}
