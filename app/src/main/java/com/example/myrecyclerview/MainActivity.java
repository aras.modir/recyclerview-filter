package com.example.myrecyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;


import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    EditText word;
    String imgUrl = "http://www.cheefood.com/wp-content/uploads/2017/07/20170112215112369-575x563.jpg";
    String imgGheyme = "https://www.aksanplus.com/news/1397/02/6big.jpg";
    String imgFesenjon = "https://ashpazbeshi.com/wp-content/uploads/2018/10/01-10.jpg";
    String imgPizza = "http://zoody.ir/tips/wp-content/uploads/2017/12/pizaa.jpg";
    private FoodsAdapter adapter;
    private List<FoodModel> foods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();

        FoodModel f1 = FoodModel.newBuilder().foodName("ghormesabzi").price(12000).restuarant("Orkide")
                .imageUrl(imgUrl).type("Sonati").build();
        FoodModel f2 = FoodModel.newBuilder().foodName("fesenjon").price(12000).restuarant("Orkide")
                .imageUrl(imgFesenjon).type("Sonati").build();
        FoodModel f3 = FoodModel.newBuilder().foodName("gheyme").price(12000).restuarant("Orkide")
                .imageUrl(imgGheyme).type("Sonati").build();
        FoodModel f4 = FoodModel.newBuilder().foodName("burger").price(12000).restuarant("Orkide")
                .imageUrl(imgUrl).type("Sonati").build();
        FoodModel f5 = FoodModel.newBuilder().foodName("pizza").price(12000).restuarant("Orkide")
                .imageUrl(imgPizza).type("Sonati").build();
        FoodModel f6 = FoodModel.newBuilder().foodName("ghormesabzi").price(12000).restuarant("Orkide")
                .imageUrl(imgUrl).type("Sonati").build();
        FoodModel f7 = FoodModel.newBuilder().foodName("fesenjon").price(12000).restuarant("Orkide")
                .imageUrl(imgFesenjon).type("Sonati").build();
        FoodModel f8 = FoodModel.newBuilder().foodName("gheyme").price(12000).restuarant("Orkide")
                .imageUrl(imgGheyme).type("Sonati").build();
        FoodModel f9 = FoodModel.newBuilder().foodName("burger").price(12000).restuarant("Orkide")
                .imageUrl(imgUrl).type("Sonati").build();
        FoodModel f10 = FoodModel.newBuilder().foodName("pizza").price(12000).restuarant("Orkide")
                .imageUrl(imgPizza).type("Sonati").build();

        foods = new ArrayList<>();
        foods.add(f1);
        foods.add(f2);
        foods.add(f3);
        foods.add(f4);
        foods.add(f5);
        foods.add(f6);
        foods.add(f7);
        foods.add(f8);
        foods.add(f9);
        foods.add(f10);

        adapter = new FoodsAdapter(this, foods);
        LinearLayoutManager lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter);

    }

    void bind() {
        recyclerView = findViewById(R.id.Recycler);
        word = findViewById(R.id.word);

        word.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterList(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    void filterList(String word) {
        List<FoodModel> filteredFoods = new ArrayList<>();
        for (FoodModel food : foods) {
            if (food.getFoodName().toLowerCase().contains(word)) {
                food.setHtChars(word);
                filteredFoods.add(food);
            }
        }

        if (filteredFoods.size() > 0)
            adapter.setItems(filteredFoods);
        else adapter.setItems(foods);
    }
}
