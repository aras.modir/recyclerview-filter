package com.example.myrecyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

public class FoodsAdapter extends RecyclerView.Adapter<FoodsAdapter.MyViewHolder> {

    Context mContext;
    List<FoodModel> foodModelList;

    public FoodsAdapter(Context mContext, List<FoodModel> foodModelList) {
        this.mContext = mContext;
        this.foodModelList = foodModelList;
    }

    public void setItems(List<FoodModel> newList) {
        foodModelList = newList;
        notifyDataSetChanged();
    }

    @Override
    public FoodsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.foods_list_item, viewGroup, false);
        MyViewHolder holder = new MyViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull FoodsAdapter.MyViewHolder myViewHolder, int i) {
        myViewHolder.name.setText(foodModelList.get(i).getFoodName());

        if (foodModelList.get(i).getHtChars().length() > 0) {

            String name = foodModelList.get(i).getFoodName();
            name = name.replaceAll(foodModelList.get(i).getHtChars().toLowerCase(),
                    "<font color = 'red'>" + foodModelList.get(i).getHtChars().toLowerCase() + "</font>");
                    myViewHolder.name.setText(Html.fromHtml(name));
        } else {
            myViewHolder.name.setText(foodModelList.get(i).getFoodName());
        }

        myViewHolder.type.setText(foodModelList.get(i).getType());
        myViewHolder.price.setText(foodModelList.get(i).getPrice() + "");
        Picasso.get().load(foodModelList.get(i).getImageUrl()).into(myViewHolder.img);

    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getItemCount() {
        return foodModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView name;
        TextView price;
        TextView type;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            type = itemView.findViewById(R.id.type);
            img = itemView.findViewById(R.id.img);

            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, foodModelList.get(getAdapterPosition()).getFoodName(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
